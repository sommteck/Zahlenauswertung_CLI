using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zahlenauswertung
{
    class Program
    {
        static void Main(string[] args)
        {
            // Variablen:
            double zahl, min, max, summe, durchschnitt, anzahl;
            // C# spezifisch, da nur texteingaben möglich
            string eingabe;
            // Zählerstartwert setzen;
            anzahl = 0;
            // Startwert summe auf 0 setzen
            summe = 0;
            try
            {
                // Benutzerführung für erste Eingabe
                Console.Write("Zahl eingeben: ");
                // Eingabe der Zahl, Datentyp string
                eingabe = Console.ReadLine();
                // Konvertieren der Eingabe zu  Datentyp Double
                zahl = Convert.ToDouble(eingabe);
                // Minimum Wert und Maximumwert erster Eingabe belegen
                // erweiterte Zuweisung, Abarbeitung von rechts nach links
                max = min = zahl;
                // Kopfgesteuerte Verarbeitungsschleife; solange Daten vorhanden sind
                while (zahl != 0)
                {
                    // Zähler um eins hochsetzen
                    anzahl++;
                    // summe ermitteln; zusammengesetze Zuweisung
                    summe += zahl;
                    // kleinsten Wert überprüfen und gegebenfalls neu setzen
                    if (zahl < min)
                    min = zahl;
                    if (zahl > max)
                    max = zahl;
                    // Benutzerführung für nächste Eingabe
                    Console.WriteLine("Zahl eingeben: ");
                    eingabe = Console.ReadLine();
                    // Eingabe der Zahl, Datentyp string
                    zahl = Convert.ToDouble(eingabe);
                }
                // Division durch 0 verhindern
                // Bedingung Divisor größer 0
                if (anzahl > 0)
                {
                    // Berechnung des Durchschnitts
                    durchschnitt = summe / anzahl;
                    // Berechnungsergebnisse ausgeben; stringverknüpfung
                    Console.WriteLine("Anzahl der Eingaben: " + anzahl.ToString("F0"));
                    Console.WriteLine("Gesamtwert aller Eingaben: " + summe.ToString("F2"));
                    Console.WriteLine("Durchschnittswert der Eingaben: " + durchschnitt.ToString("F2"));
                    Console.WriteLine("Kleinste eingegeben Zahl: " + min.ToString("F2"));
                    Console.WriteLine("Größte eingegebebene Zahl: " + max.ToString("F2"));
                }
                else
                    Console.WriteLine("Division durch 0 nicht definiert!");
                    Console.Write("... weiter mit beliebiger Taste.");
                    Console.ReadKey();
            }
            catch
            {
                Console.WriteLine("Nur Zahlen eingeben!");
                Console.WriteLine("...weiter mit beliebiger Taste.");
                Console.ReadKey();
            }
        }
    }
}
